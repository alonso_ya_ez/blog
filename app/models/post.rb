class Post < ActiveRecord::Base
	# esta linea es para indicar que cada comentario depende de un post y al borrar un post se eliminan los comentarios
	has_many :comments, dependent: :destroy   
	validates_length_of :title, :in => 5..20, :message => "longitud no valida" # esta linea es para indicar la longuitud de la cadena
	validates_presence_of :body # esta linea es para indicar que el campo es requerido
end

