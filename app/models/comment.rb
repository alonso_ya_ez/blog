class Comment < ActiveRecord::Base
	belongs_to :"post"# esta linea es para indicar que cada comentario depende de un post y al borrar un post se eliminan los comentarios
	validates_numericality_of :post_id, :only_ingeter => true # esta linea es para indicar que el unico tipo de dato aceptado es numerico y entero
	validates_presence_of :body # esta linea es para indicar que el campo es requerido
	validates_presence_of :post_id # esta linea es para indicar que el campo es requerido
end
